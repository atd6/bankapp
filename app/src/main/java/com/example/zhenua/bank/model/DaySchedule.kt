package com.example.zhenua.bank.model
data class AtmTime (val result: OpenHours)
data class  OpenHours (val opening_hours: DaySchedule)
data class DaySchedule(val weekday_text:List<String>)

