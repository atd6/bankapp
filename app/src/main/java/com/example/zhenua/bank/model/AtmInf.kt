package com.example.zhenua.bank.model

data class AtmInf(val results: List<AtmItemInf> = ArrayList())

data class AtmItemInf(val geometry: Location,
                      val opening_hours: OpenCheck?,
                      val place_id: String,
                      val types: List<String>,
                      val vicinity: String
)
data class Location(val location: Map<String,Double>)
data class OpenCheck(val open_now: Boolean)
