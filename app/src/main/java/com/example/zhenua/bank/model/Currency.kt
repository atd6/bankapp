package com.example.zhenua.bank.model

data class NameOfCurrency(val symbols: Map<String,String> = HashMap())

data class Currency(val rates: Map<String, Float> = HashMap() )


