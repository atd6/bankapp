package com.example.zhenua.bank

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zhenua.bank.adapter.MapAdapter
import com.example.zhenua.bank.model.AtmItem
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_maps.*


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, MapAdapter.OnBankClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private val adapter = MapAdapter(this)
    private val repository = MapRepository()
    private lateinit var atmInfList: List<AtmItem>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment

        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        createRecycler()

    }
    private fun createRecycler()
    {
        if (checkPermission()) {
            recyclerInit()
            fusedLocationClient.lastLocation.addOnSuccessListener(this)
            {
                if (it != null) {
                    lastLocation = it
                    val currentLatLng = LatLng(lastLocation.latitude, lastLocation.longitude)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 17f))
                    fillRecycler()
                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isZoomControlsEnabled = true
        includeLocation()
    }
    private fun includeLocation()
    {
        if(checkPermission())
        {
            mMap.isMyLocationEnabled = true
        }
    }

    override fun onBankClick(item: AtmItem) {
        val lat = item.coordinats.location["lat"]
        val lng = item.coordinats.location["lng"]
        if (lat != null && lng != null) {
            val objectLatLng = LatLng(lat,lng)
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(objectLatLng, 17f))
        }
    }

    private fun checkPermission(): Boolean
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION)
            return false
        }
        else return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            when (requestCode)
            {
                LOCATION_PERMISSION -> {createRecycler()
                    includeLocation()
                }
            }
        }
    }
    private fun fillRecycler()
    {
            atmInfList = repository.getAllAtm("${lastLocation.latitude},${lastLocation.longitude}")
            createAtmMarker()
            adapter.setInf(atmInfList)
    }

    private fun createAtmMarker()
    {
        atmInfList.forEach {
            val lat = it.coordinats.location["lat"]
            val lng = it.coordinats.location["lng"]
            if ( lat != null &&  lng != null) {
                val latLng = LatLng(lat, lng)
                val marker = MarkerOptions().position(latLng)
                marker.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(resources, R.drawable.map_pin)))
                mMap.addMarker(marker)
            }
        }
    }

    private fun recyclerInit() {
        BankomatRecycler.layoutManager = LinearLayoutManager(this)
        BankomatRecycler.adapter = adapter
    }

    companion object {
        private const val LOCATION_PERMISSION = 1
    }
}
