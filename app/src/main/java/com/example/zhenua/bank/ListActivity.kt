package com.example.zhenua.bank


import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zhenua.bank.adapter.ListAdapter
import com.example.zhenua.bank.dateUtils.DateUtils
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    private val adapter = ListAdapter()
    private  val repos = ExchangeRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        dateText.text = DateUtils.getTodayDate()
        initRecyclerView()
        val itemList= repos.getAllInf()
        adapter.setRates(itemList)
    }


    private fun initRecyclerView()
    {
        recView.layoutManager = LinearLayoutManager(this)
        recView.adapter = adapter
    }

}
