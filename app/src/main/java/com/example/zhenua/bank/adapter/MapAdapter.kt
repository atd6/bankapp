package com.example.zhenua.bank.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.zhenua.bank.R
import com.example.zhenua.bank.model.AtmItem
import kotlinx.android.synthetic.main.bankomat_item.view.*

class MapAdapter(private val listener: OnBankClickListener): RecyclerView.Adapter<MapAdapter.MapViewHolder>() {
    private var atmList: List<AtmItem> = ArrayList()


    fun setInf(atm: List<AtmItem>)
    {
        atmList = atm
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MapViewHolder {
        val view:View = LayoutInflater.from(parent.context).inflate(R.layout.bankomat_item, parent , false)
            view.setOnClickListener{}
        return MapViewHolder(view)
    }

    override fun onBindViewHolder(holder: MapViewHolder, position: Int) {
        holder.bind(atmList[position])

    }

    override fun getItemCount(): Int {
        return atmList.size
    }

    inner class MapViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener
    {
        init
        {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != -1){
                listener.onBankClick(atmList[position])
            }
        }

        fun bind(item: AtmItem)
        {
            itemView.addressInf.text = item.address
            itemView.bankType.text = item.typeOfAtm
            val time = String.format(itemView.resources.getString(R.string.time), item.time)
            itemView.timeInf.text = time

            if (item.open == null)
            {
                itemView.openCheck.text = itemView.resources.getString(R.string.noInf)
                val color = ContextCompat.getColor(itemView.context, R.color.mapGrayText)
                itemView.openCheck.setTextColor(color)
            }
            else {
                if (item.open) {
                    itemView.openCheck.text = itemView.resources.getString(R.string.open)
                    val color = ContextCompat.getColor(itemView.context, R.color.mapBankOpen)
                    itemView.openCheck.setTextColor(color)
                } else {
                    itemView.openCheck.text = itemView.resources.getString(R.string.close)
                    val color = ContextCompat.getColor(itemView.context, R.color.MapBankClosed)
                    itemView.openCheck.setTextColor(color)
                }
            }
        }
    }
    interface OnBankClickListener
    {
        fun onBankClick(item: AtmItem)
    }
}