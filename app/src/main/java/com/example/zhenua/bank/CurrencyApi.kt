package com.example.zhenua.bank

import com.example.zhenua.bank.model.Currency
import com.example.zhenua.bank.model.NameOfCurrency
import com.example.zhenua.bank.model.accessKey
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CurrencyApi {
    @GET("symbols")
    fun getNames(@Query("access_key") key: String = accessKey): Call<NameOfCurrency>
    @GET("latest")
    fun getCourse(@Query("access_key") key: String = accessKey): Call<Currency>
    @GET("{date}")
    fun getYestCourse(@Path("date") date: String, @Query("access_key") key: String = accessKey): Call<Currency>

}