package com.example.zhenua.bank

import com.example.zhenua.bank.dateUtils.DateUtils
import com.example.zhenua.bank.model.AtmInf
import com.example.zhenua.bank.model.AtmItem
import com.example.zhenua.bank.model.AtmItemInf
import com.example.zhenua.bank.retrofitBuilders.MapApiBuilder
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlin.collections.ArrayList

class MapRepository {

    private val site = MapApiBuilder().site
    fun getAllAtm(location: String): List<AtmItem>
    {
        var inf = AtmInf()
        runBlocking { inf = getATM(location) }
        val time = getTime(inf.results)
        val atmList = ArrayList<AtmItem>()
        for (i in 0 until inf.results.size)
        {
            val type: String = if (inf.results[i].types.contains("bank")) "Отделение"
            else "Банкомат"
            val open = inf.results[i].opening_hours
            if(open == null)
            {
                atmList.add(AtmItem(inf.results[i].vicinity,
                    time[i],
                    type,
                    inf.results[i].geometry,
                    null
                ))
            }
            else
            {
                atmList.add(AtmItem(inf.results[i].vicinity,
                    time[i],
                    type,
                    inf.results[i].geometry,
                    open.open_now
                ))
            }


        }
        return atmList


    }
   private fun getTime(InfList: List<AtmItemInf>): List<String>
    {
        val timeList = ArrayList<String>()
        val today = DateUtils.getTodayDay()
        runBlocking {
            InfList.forEach {
                if (it.opening_hours == null)
                {
                    timeList.add("Неизвестно")
                }
                else
                {
                    val atmTime = getOpenTime(it.place_id)
                    if(atmTime != null)
                    {
                        val time =
                            atmTime.result.opening_hours.weekday_text[today].substringAfter(':')
                        timeList.add(time)
                    }
                }
            }
        }
        return timeList
    }
    private suspend fun getATM(location: String) = withContext(IO){ site.getNearbyAtm(location).execute()}.body() ?: AtmInf()
    private suspend fun  getOpenTime(id: String) = withContext(IO) {site.getTimeInf(id).execute()}.body()
}