package com.example.zhenua.bank

import android.content.Intent

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.zhenua.bank.dateUtils.DateUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.runBlocking


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dataText.text = DateUtils.getTodayDate()

        val exRep = ExchangeRepository()
        val list = exRep.getMainRate()
        USDcurrency.text = list["USD"].toString()
        EURcurrency.text = list["EUR"].toString()




        currencyButton.setOnClickListener()
        {
            val intent = Intent(this@MainActivity, ListActivity::class.java)
            startActivity(intent)
        }
        bankomatButton.setOnClickListener()
        {
            val intent = Intent(this@MainActivity, MapsActivity::class.java)
            startActivity(intent)
        }

    }
}
