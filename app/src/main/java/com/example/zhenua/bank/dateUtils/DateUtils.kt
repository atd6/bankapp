package com.example.zhenua.bank.dateUtils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {
    fun getTodayDate(): String
    {
        val date = Date()
        val dateFormat = SimpleDateFormat("dd.MM.YY", Locale.getDefault())
        return dateFormat.format(date).toString()
    }
    fun getYesterdayDate(): String
    {
        val cal = Calendar.getInstance()
        val date = Date()
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        cal.time =  dateFormat.parse(dateFormat.format(date))
        cal.add(Calendar.DATE, -1)
        return dateFormat.format(cal.time)
    }
    fun getTodayDay(): Int
    {
        val calendar: Calendar = Calendar.getInstance()
        calendar.time = Date()
        var day = calendar.get(Calendar.DAY_OF_WEEK)
        return if (day == 1) 6 else day-2
    }
}