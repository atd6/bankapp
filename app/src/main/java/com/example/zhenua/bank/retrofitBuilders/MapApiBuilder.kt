package com.example.zhenua.bank.retrofitBuilders

import com.example.zhenua.bank.MapApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MapApiBuilder {
    val site: MapApi

    init
    {
        val server =
            Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/place/").addConverterFactory(
                GsonConverterFactory.create()).build()
        site = server.create(MapApi::class.java)
    }
}