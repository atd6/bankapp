package com.example.zhenua.bank.model

data class ExchangeItemList( var moneyList: Currency,
          var yestMoneyList: Currency,
         var keyNames: List<String>,
         var fullName: NameOfCurrency)
