package com.example.zhenua.bank

import com.example.zhenua.bank.retrofitBuilders.RateApiBuilder
import com.example.zhenua.bank.dateUtils.DateUtils
import com.example.zhenua.bank.model.*
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlin.math.roundToInt

class ExchangeRepository {
    private val site = RateApiBuilder().site

    private lateinit var todayRate: Currency
    private lateinit var yesterdayRate: Currency
    private lateinit var keyNames:List<String>
    private lateinit var fullNames: NameOfCurrency

    private suspend fun getCourse() = withContext(IO) {
        site.getCourse().execute()
    }.body() ?: Currency()
    private  suspend fun getNames() = withContext(IO){
        site.getNames().execute()
    }.body() ?: NameOfCurrency()
    private  suspend fun getYesterdayCourse() = withContext(IO){
        site.getYestCourse(DateUtils.getYesterdayDate()).execute()
    }.body() ?: Currency()

    fun getAllInf(): List<CurrencyItem>
    {
        runBlocking{

            fullNames = getNames()
            keyNames = ArrayList<String>(fullNames.symbols.keys)
            todayRate = getCourse()
            yesterdayRate = getYesterdayCourse()
        }
        val list = ArrayList<CurrencyItem>()
        keyNames.forEach{
            val item = CurrencyItem(
                adaptToRub(todayRate.rates.getValue("RUB"), todayRate.rates.getValue(it)),
                it,
                adaptToRub(yesterdayRate.rates.getValue("RUB"), yesterdayRate.rates.getValue(it)),
                fullNames.symbols.getValue(it)
            )
            list.add(item)
        }
        return list
    }

    fun  getMainRate(): Map<String, Float>
    {
        val map:HashMap<String, Float> = HashMap()
        runBlocking {
                todayRate = getCourse()
        }
            map["USD"] = adaptToRub(
                (todayRate.rates.getValue("RUB")),
                todayRate.rates.getValue("USD")
            )
            map["EUR"] = (todayRate.rates.getValue("RUB") * 100).roundToInt().toFloat() / 100
        return  map
    }

    private fun adaptToRub(rubRate:  Float, currencyRate: Float): Float
    {
        var p = rubRate / currencyRate * 100
        p = (p.roundToInt()).toFloat() / 100
        return p
    }







}