package com.example.zhenua.bank.model


data class CurrencyItem(val todayCost: Float,
                        val shortName: String,
                        val yesterdayCost: Float,
                        val fullName: String
)


