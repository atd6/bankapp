package com.example.zhenua.bank.model

data class AtmItem (val address: String,
                    val time: String,
                    val typeOfAtm: String,
                    val coordinats: Location,
                    val open: Boolean?
)