package com.example.zhenua.bank.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zhenua.bank.R
import com.example.zhenua.bank.model.CurrencyItem
import kotlinx.android.synthetic.main.money_item.view.*


 class ListAdapter : RecyclerView.Adapter<ListAdapter.RecViewHolder>() {

    private var moneyList: List<CurrencyItem> = ArrayList()

     fun setRates(list: List<CurrencyItem>)
     {
         moneyList = list
         notifyDataSetChanged()
     }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecViewHolder {
        val view: View = LayoutInflater.from(p0.context).inflate(R.layout.money_item, p0, false)
        return RecViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecViewHolder, position: Int) {
        holder.bind(moneyList[position])
    }

    override fun getItemCount(): Int {
        return moneyList.size
    }

    inner class RecViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind( item: CurrencyItem)
        {
            itemView.nameCur.text = item.shortName
            itemView.fullNameCur.text =  item.fullName
            itemView.buyCost.text =  item.todayCost.toString()
            itemView.sellCost.text =  item.todayCost.toString()

            if (moneyList[position].todayCost >  item.yesterdayCost) {
                itemView.buyImage.setImageResource(R.drawable.up)
                itemView.sellImage.setImageResource(R.drawable.up)
            } else {
                itemView.buyImage.setImageResource(R.drawable.down)
                itemView.sellImage.setImageResource(R.drawable.down)
            }
            if (item.todayCost ==  item.yesterdayCost) {
                itemView.buyImage.visibility = View.INVISIBLE
                itemView.sellImage.visibility = View.INVISIBLE
            }
            else {
                itemView.buyImage.visibility = View.VISIBLE
                itemView.sellImage.visibility = View.VISIBLE
            }
        }
    }
}

