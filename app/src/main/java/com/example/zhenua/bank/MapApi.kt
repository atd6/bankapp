package com.example.zhenua.bank

import com.example.zhenua.bank.model.AtmInf
import com.example.zhenua.bank.model.AtmTime
import com.example.zhenua.bank.model.mapKey
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MapApi {
    @GET("nearbysearch/json?language=ru")
    fun getNearbyAtm(@Query("location") location: String,
                     @Query("radius") radius: String = "2000",
                     @Query("type") type :String = "atm",
                     @Query("key") key: String = mapKey): Call<AtmInf>

    @GET("details/json?language=ru&fields=opening_hours")
    fun getTimeInf(@Query("place_id") id:String, @Query("key") key: String = mapKey): Call<AtmTime>
}