package com.example.zhenua.bank.retrofitBuilders

import com.example.zhenua.bank.CurrencyApi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RateApiBuilder {
    val site: CurrencyApi
    init
    {
        val server =
            Retrofit.Builder().baseUrl("http://data.fixer.io/api/").addConverterFactory(GsonConverterFactory.create()).build()
        site = server.create(CurrencyApi::class.java)
    }
}